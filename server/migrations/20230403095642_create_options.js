/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("options", (t) => {
    t.uuid("uuid").primary();
    t.uuid("question_uuid").notNullable();
    t.foreign("question_uuid").references("questions.uuid");
    t.string("option").notNullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("options");
};
