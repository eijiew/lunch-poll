/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("questions", (t) => {
    t.uuid("uuid").primary();
    t.string("question").notNullable();
    t.string("created_by").notNullable();
    t.string("resulted_option_uuid");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("questions");
};
