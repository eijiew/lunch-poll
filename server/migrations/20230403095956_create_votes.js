/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("votes", (t) => {
    t.uuid("option_uuid").notNullable();
    t.foreign("option_uuid").references("options.uuid");
    t.string("username").notNullable();

    t.primary(["option_uuid", "username"]);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("votes");
};
