/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  setupFiles: ["<rootDir>/src/test/helper.js"],
  testPathIgnorePatterns: ["/node_modules/", "/dist/"]
};
