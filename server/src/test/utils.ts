const response: any = {
  status: jest.fn(),
  json: jest.fn(),
  sendStatus: jest.fn(),
};

Object.keys(response).reduce(
  (agg, key) => ({ ...agg, [key]: response[key].mockReturnValue(response) }),
  {}
);

export const responseMock: any = Object.keys(response).reduce(
  (agg, key) => ({ ...agg, [key]: response[key].mockReturnValue(response) }),
  {}
);
