const Knex = require("knex");
const dbConfig = require("../../knexfile");
const { Model } = require("objection");
const knex = Knex(dbConfig);
Model.knex(knex);
