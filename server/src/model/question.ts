import { Model } from "objection";
import { Option } from "./option";

export class Question extends Model {
  static get tableName() {
    return "questions";
  }
  static get idColumn() {
    return "uuid";
  }

  static get relationMappings() {
    return {
      options: {
        relation: Model.HasManyRelation,
        modelClass: Option,
        join: {
          from: "questions.uuid",
          to: "options.question_uuid",
        },
      },
      resulted_option: {
        relation: Model.HasOneRelation,
        modelClass: Option,
        join: {
          from: "questions.resulted_option_uuid",
          to: "options.uuid",
        },
      },
    };
  }

  public readonly uuid!: string;
  public readonly question!: string;
  public readonly created_by!: string;
  public readonly options?: Option[];
  public readonly resulted_option?: Option;
  public readonly resulted_option_uuid?: string | null;
}
