import { Model } from "objection";
import { Option } from "./option";

export class Vote extends Model {
  static get tableName() {
    return 'votes';
  }
  static get idColumn() {
    return ['username', 'option_uuid']
  }

  static get relationMappings() {
    return {
      option: {
        relation: Model.BelongsToOneRelation,
        modelClass: Option,
        join: {
          from: 'option_uuid',
          to: 'options.uuid'
        }
      }
    };
  }
}
