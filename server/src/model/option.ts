import { Model } from "objection";
import { Question } from "./question";
import { Vote } from "./vote";

export class Option extends Model {
  static get tableName() {
    return 'options';
  }
  static get idColumn() {
    return 'uuid'
  }

  static get relationMappings() {
    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'question_uuid',
          to: 'questions.uuid'
        }
      },
      votes: {
        relation: Model.HasManyRelation,
        modelClass: Vote,
        join: {
          from: 'options.uuid',
          to: 'votes.option_uuid'
        }
      }
    };
  }

  public readonly uuid!: string;
  public readonly option!: string;
  public readonly question?: Question;
  public readonly votes?: Vote[];
}
