export default {
  openapi: "3.0.0",
  info: {
    title: "Lunch Poll API",
    version: "1.0.0",
  },
  paths: {
    "/questions/{uuid}": {
      get: {
        operationId: "getQuestion",
        description: "get question by identifier",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of a Question",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  required: ["uuid", "question", "created_by"],
                  properties: {
                    uuid: {
                      type: "string",
                    },
                    question: {
                      type: "string",
                    },
                    created_by: {
                      type: "string",
                    },
                    resulted_option: {
                      type: "object",
                      required: ["uuid", "option"],
                      properties: {
                        uuid: {
                          type: "string",
                        },
                        option: {
                          type: "string",
                        },
                      },
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
    },
    "/questions": {
      post: {
        operationId: "postQuestion",
        description: "create a new polling question",
        requestBody: {
          description: "Question to add",
          required: true,
          content: {
            "application/json": {
              schema: {
                type: "object",
                required: ["question", "created_by"],
                properties: {
                  question: {
                    type: "string",
                  },
                  created_by: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  required: ["uuid", "question", "created_by"],
                  properties: {
                    uuid: {
                      type: "string",
                    },
                    question: {
                      type: "string",
                    },
                    created_by: {
                      type: "string",
                    },
                  },
                },
              },
            },
          },
          "500": {
            description: "Internal Server Error",
          },
        },
      },
    },
    "/questions/{uuid}/options": {
      get: {
        operationId: "getOptions",
        description: "get list of options of a question",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of a Question",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "array",
                  items: {
                    type: "object",
                    required: ["uuid", "option"],
                    properties: {
                      uuid: {
                        type: "string",
                      },
                      option: {
                        type: "string",
                      },
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
      post: {
        operationId: "postOption",
        description: "add new option to a question",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of a Question",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        requestBody: {
          description: "Option to add",
          required: true,
          content: {
            "application/json": {
              schema: {
                type: "object",
                required: ["option"],
                properties: {
                  option: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  required: ["uuid", "option"],
                  properties: {
                    uuid: {
                      type: "string",
                    },
                    option: {
                      type: "string",
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
    },
    "/options/{uuid}/votes": {
      get: {
        operationId: "getVotes",
        description: "get a list of voters details for an option",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of an Option",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "array",
                  items: {
                    type: "object",
                    required: ["username"],
                    properties: {
                      username: {
                        type: "string",
                      },
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
      post: {
        operationId: "postVote",
        description: "add vote for an option",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of an Option",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        requestBody: {
          description: "Vote to add",
          required: true,
          content: {
            "application/json": {
              schema: {
                type: "object",
                required: ["username"],
                properties: {
                  username: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "object",
                  required: ["username"],
                  properties: {
                    username: {
                      type: "string",
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
      delete: {
        operationId: "deleteVote",
        description: "remove vote for an option",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of an Option",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        requestBody: {
          description: "Vote to delete",
          required: true,
          content: {
            "application/json": {
              schema: {
                type: "object",
                required: ["username"],
                properties: {
                  username: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
        responses: {
          "204": {
            description: "No Content",
          },
          "404": {
            description: "Not Found",
          },
        },
      },
    },
    "/questions/{uuid}/result": {
      post: {
        operationId: "postQuestionResult",
        description: "close the poll question and populate result",
        parameters: [
          {
            in: "path",
            name: "uuid",
            description: "Unique identifier of a Question",
            required: true,
            schema: {
              type: "string",
            },
          },
        ],
        requestBody: {
          description: "User details",
          required: true,
          content: {
            "application/json": {
              schema: {
                type: "object",
                required: ["username"],
                properties: {
                  username: {
                    type: "string",
                  },
                },
              },
            },
          },
        },
        responses: {
          "200": {
            description: "OK",
            content: {
              "application/json": {
                schema: {
                  type: "array",
                  items: {
                    type: "object",
                    required: ["uuid", "option"],
                    properties: {
                      uuid: {
                        type: "string",
                      },
                      option: {
                        type: "string",
                      },
                    },
                  },
                },
              },
            },
          },
          "404": {
            description: "Not Found",
          },
        },
      },
    },
  },
};
