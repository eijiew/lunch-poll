import { Request, Response } from "express";
import { Option } from "../../model/option";
import { Vote } from "../../model/vote";

export const postVote = async (req: Request, res: Response) => {
  try {
    const { uuid: optionUuid } = req.params;
    const body = req.body;

    let vote = await Vote.query().findById([body.username, optionUuid]);
    if (!vote) {
      vote = await Option.relatedQuery("votes").for(optionUuid).insertAndFetch({
        username: body.username,
      });
    }
    res.status(200).json(vote);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
