import { Request, Response } from "express";
import { Option } from "../../model/option";

export const getVotes = async (req: Request, res: Response) => {
  try {
    const { uuid } = req.params;
    const { votes } = await Option.query()
      .findById(uuid)
      .withGraphFetched("votes")
      .throwIfNotFound();
    res.status(200).json(votes);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
