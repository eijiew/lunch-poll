import { Request, Response } from "express";
import { randomUUID } from "crypto";
import { Question } from "../../model/question";

export const postOption = async (req: Request, res: Response) => {
  try {
    const { uuid: questionUuid } = req.params;
    const question = await Question.query()
      .findById(questionUuid)
      .whereNull("resulted_option_uuid")
      .throwIfNotFound();

    const body = req.body;
    const option = await Question.relatedQuery("options")
      .for(questionUuid)
      .insertAndFetch({
        uuid: randomUUID().toString(),
        option: body.option,
      });
    res.status(200).json(option);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
