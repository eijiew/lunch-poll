import { Option } from "../../../model/option";
import { Question } from "../../../model/question";
import { Vote } from "../../../model/vote";
import { responseMock } from "../../../test/utils";
import { getVotes } from "../getVotes";

describe("getVotes", () => {
  const question = {
    uuid: "question-uuid-for-get-votes",
    question: "some question",
    created_by: "foo bar",
  };
  const option = {
    uuid: "option-uuid-for-get-votes",
    option: "option 1",
    question_uuid: question.uuid,
  };
  const votes = [
    {
      username: "foo bar",
      option_uuid: option.uuid,
    },
    {
      username: "foo baz",
      option_uuid: option.uuid,
    },
  ];
  beforeAll(async () => {
    await Question.query().insert(question);
    await Option.query().insert(option);
    for (const vote of votes) {
      await Vote.query().insert(vote);
    }
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    await Option.query().deleteById(option.uuid);
    for (const vote of votes) {
      await Vote.query().deleteById([vote.username, vote.option_uuid]);
    }
  });

  it("should retrieve votes and return", async () => {
    const request: any = { params: { uuid: option.uuid } };
    await getVotes(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);
    expect(responseMock.json).toHaveBeenLastCalledWith(votes);
  });

  it("should return status 404 when option not found", async () => {
    const request: any = { params: { uuid: "unknown-id" } };
    await getVotes(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
