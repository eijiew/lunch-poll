import { Question } from "../../../model/question";
import { responseMock } from "../../../test/utils";
import { getQuestion } from "../getQuestion";

describe("getQuestion", () => {
  const question = {
    uuid: "question-uuid",
    question: "some question",
    created_by: "foo bar",
  };
  beforeAll(async () => {
    await Question.query().insert(question);
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
  });

  it("should retrieve question and return", async () => {
    const request: any = { params: { uuid: question.uuid } };
    await getQuestion(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);
    expect(responseMock.json).toHaveBeenLastCalledWith({
      ...question,
      resulted_option_uuid: null,
      resulted_option: null,
    });
  });

  it("should return status 404 when question not found", async () => {
    const request: any = { params: { uuid: "unknown-id" } };
    await getQuestion(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
