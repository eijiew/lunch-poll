import { Question } from "../../../model/question";
import { Option } from "../../../model/option";
import { responseMock } from "../../../test/utils";
import { postQuestionResult } from "../postQuestionResult";

describe("postQuestionResult", () => {
  const question = {
    uuid: "question-uuid-for-get-question-result",
    question: "some question",
    created_by: "foo bar",
  };
  const options = [
    {
      uuid: "option-uuid-for-post-question-result-1",
      option: "option 1",
      question_uuid: question.uuid,
    },
    {
      uuid: "option-uuid-for-post-question-result-2",
      option: "option 2",
      question_uuid: question.uuid,
    },
  ];
  beforeAll(async () => {
    await Question.query().insert(question);
    for (const option of options) {
      await Option.query().insert(option);
    }
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    for (const option of options) {
      await Option.query().deleteById(option.uuid);
    }
  });

  it("should return status 404 when question is not owned by requestor", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { username: "unknown" },
    };
    await postQuestionResult(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });

  it("should randomly pick the option as result and return", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { username: question.created_by },
    };
    await postQuestionResult(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);
    const resultedOption = responseMock.json.mock.lastCall[0];
    expect(Object.keys(resultedOption)).toEqual([
      "uuid",
      "question_uuid",
      "option",
    ]);
  });

  it("should return status 404 when question already have got result", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { username: question.created_by },
    };
    await postQuestionResult(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });

  it("should return status 404 when question got no options", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { username: question.created_by },
    };

    await Question.query().update({ resulted_option_uuid: null });
    for (const option of options) {
      await Option.query().deleteById(option.uuid);
    }

    await postQuestionResult(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
