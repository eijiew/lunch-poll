import { Option } from "../../../model/option";
import { Question } from "../../../model/question";
import { responseMock } from "../../../test/utils";
import { getOptions } from "../getOptions";

describe("getOptions", () => {
  const question = {
    uuid: "question-uuid-for-get-options",
    question: "some question",
    created_by: "foo bar",
  };
  const options = [
    {
      uuid: "option-1",
      option: "option 1",
      question_uuid: question.uuid,
    },
    {
      uuid: "option-2",
      option: "option 2",
      question_uuid: question.uuid,
    },
  ];
  beforeAll(async () => {
    await Question.query().insert(question);
    for (const option of options) {
      await Option.query().insert(option);
    }
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    for (const option of options) {
      await Option.query().deleteById(option.uuid);
    }
  });

  it("should retrieve options and return", async () => {
    const request: any = { params: { uuid: question.uuid } };
    await getOptions(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);
    expect(responseMock.json).toHaveBeenLastCalledWith(options);
  });

  it("should return status 404 when question not found", async () => {
    const request: any = { params: { uuid: "unknown-id" } };
    await getOptions(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
