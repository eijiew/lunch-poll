import { Option } from "../../../model/option";
import { Question } from "../../../model/question";
import { responseMock } from "../../../test/utils";
import { postOption } from "../postOption";

describe("postOption", () => {
  let insertedUuid: string;
  const question = {
    uuid: "question-uuid-for-post-option",
    question: "some question",
    created_by: "foo bar",
  };
  beforeAll(async () => {
    await Question.query().insert(question);
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    await Option.query().deleteById(insertedUuid);
  });

  it("should insert option", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { option: "foo bar baz" },
    };
    await postOption(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);

    const insertedOption = responseMock.json.mock.lastCall[0];
    expect(insertedOption.option).toEqual(request.body.option);
    insertedUuid = insertedOption.uuid;
  });

  it("should return 404 when question has gotten result", async () => {
    const request: any = {
      params: { uuid: question.uuid },
      body: { option: "foo bar baz" },
    };

    await Question.query().update({resulted_option_uuid: 'lala'})

    await postOption(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
