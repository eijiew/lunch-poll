import { Option } from "../../../model/option";
import { Question } from "../../../model/question";
import { Vote } from "../../../model/vote";
import { responseMock } from "../../../test/utils";
import { postVote } from "../postVote";

describe("postVote", () => {
  let insertedUuid: string;
  const question = {
    uuid: "question-uuid-for-post-vote",
    question: "some question",
    created_by: "foo bar",
  };
  const option = {
    uuid: "option-uuid-for-post-vote",
    option: "option 1",
    question_uuid: question.uuid,
  };
  beforeAll(async () => {
    await Question.query().insert(question);
    await Option.query().insert(option);
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    await Option.query().deleteById(option.uuid);
    await Vote.query().deleteById(["foo bar", option.uuid]);
  });

  it("should insert vote", async () => {
    const request: any = {
      params: { uuid: option.uuid },
      body: { username: "foo bar" },
    };
    const voteCountBeforeInsert = await Vote.query()
      .where({ username: "foo bar", option_uuid: option.uuid })
      .count({ count: "*" })
      .first();
    expect(voteCountBeforeInsert).toEqual({ count: 0 });

    await postVote(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);

    const voteCountAfterInsert = await Vote.query()
      .where({ username: "foo bar", option_uuid: option.uuid })
      .count({ count: "*" })
      .first();
    expect(voteCountAfterInsert).toEqual({ count: 1 });
  });
});
