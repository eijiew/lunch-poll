import { Question } from "../../../model/question";
import { responseMock } from "../../../test/utils";
import { postQuestion } from "../postQuestion";

describe("postQuestion", () => {
  let insertedUuid: string;
  afterAll(async () => {
    await Question.query().deleteById(insertedUuid);
  });

  it("should insert question", async () => {
    const request: any = {
      body: { question: "foo bar baz", created_by: "foo bar" },
    };
    await postQuestion(request, responseMock);

    expect(responseMock.status).toHaveBeenLastCalledWith(200);

    const insertedQuestion = responseMock.json.mock.lastCall[0];
    expect(insertedQuestion.question).toEqual(request.body.question);
    expect(insertedQuestion.created_by).toEqual(request.body.created_by);
    insertedUuid = insertedQuestion.uuid;
  });
});
