import { Option } from "../../../model/option";
import { Question } from "../../../model/question";
import { Vote } from "../../../model/vote";
import { responseMock } from "../../../test/utils";
import { deleteVote } from "../deleteVote";

describe("deleteVote", () => {
  const question = {
    uuid: "question-uuid-for-del-votes",
    question: "some question",
    created_by: "foo bar",
  };
  const option = {
    uuid: "option-uuid-for-del-votes",
    option: "option 1",
    question_uuid: question.uuid,
  };
  const votes = [
    {
      username: "foo bar",
      option_uuid: option.uuid,
    },
    {
      username: "foo baz",
      option_uuid: option.uuid,
    },
  ];
  beforeAll(async () => {
    await Question.query().insert(question);
    await Option.query().insert(option);
    for (const vote of votes) {
      await Vote.query().insert(vote);
    }
  });
  afterAll(async () => {
    await Question.query().deleteById(question.uuid);
    await Option.query().deleteById(option.uuid);
    for (const vote of votes) {
      await Vote.query().deleteById([vote.username, vote.option_uuid]);
    }
  });

  it("should delete vote and return", async () => {
    const request: any = {
      params: { uuid: option.uuid },
      body: { username: "foo baz" },
    };

    const voteCountBeforeDelete = await Vote.query()
      .where({ option_uuid: option.uuid })
      .count({ count: "*" })
      .first();
    expect(voteCountBeforeDelete).toEqual({ count: 2 });

    await deleteVote(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(204);

    const voteCountAfterDelete = await Vote.query()
      .where({ option_uuid: option.uuid })
      .count({ count: "*" })
      .first();
    expect(voteCountAfterDelete).toEqual({ count: 1 });
  });

  it("should return status 404 when vote not found", async () => {
    const request: any = {
      params: { uuid: option.uuid },
      body: { username: "lalala" },
    };
    await deleteVote(request, responseMock);

    expect(responseMock.sendStatus).toHaveBeenLastCalledWith(404);
  });
});
