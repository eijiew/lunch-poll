import { Request, Response } from "express";
import { Question } from "../../model/question";
import { randomUUID } from "crypto";

export const postQuestion = async (req: Request, res: Response) => {
  try {
    const body = req.body;
    const question = await Question.query().insertAndFetch({
      uuid: randomUUID().toString(),
      question: body.question,
      created_by: body.created_by,
    });
    res.status(200).json(question);
  } catch (error) {
    console.error({ error });
    res.sendStatus(500);
  }
};
