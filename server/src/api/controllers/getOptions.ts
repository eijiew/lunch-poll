import { Request, Response } from "express";
import { Question } from "../../model/question";

export const getOptions = async (req: Request, res: Response) => {
  try {
    const { uuid } = req.params;
    const { options } = await Question.query()
      .findById(uuid)
      .withGraphFetched("options")
      .throwIfNotFound();
    res.status(200).json(options);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
