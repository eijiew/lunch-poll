import { Request, Response } from "express";
import { Vote } from "../../model/vote";

export const deleteVote = async (req: Request, res: Response) => {
  try {
    const { uuid: optionUuid } = req.params;
    const body = req.body;

    await Vote.query()
      .deleteById([body.username, optionUuid])
      .throwIfNotFound();
    res.sendStatus(204);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
