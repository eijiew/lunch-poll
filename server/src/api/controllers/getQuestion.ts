import { Request, Response } from "express";
import { Question } from "../../model/question";

export const getQuestion = async (req: Request, res: Response) => {
  try {
    const { uuid } = req.params;
    const question = await Question.query()
      .findById(uuid)
      .withGraphFetched("resulted_option")
      .throwIfNotFound();
    res.status(200).json(question);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
