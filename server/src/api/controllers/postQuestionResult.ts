import { Request, Response } from "express";
import { Question } from "../../model/question";
import { randomUUID } from "crypto";
import { NotFoundError } from "objection";

export const postQuestionResult = async (req: Request, res: Response) => {
  try {
    const { uuid } = req.params;
    const body = req.body;
    const { options } = await Question.query()
      .findById(uuid)
      .where({ created_by: body.username })
      .whereNull("resulted_option_uuid")
      .withGraphFetched("options")
      .throwIfNotFound();

    if (!options?.length) {
      throw NotFoundError;
    }
    const selectedIndex = Math.floor(Math.random() * options.length);
    await Question.query().update({
      resulted_option_uuid: options[selectedIndex].uuid,
    });
    res.status(200).json(options[selectedIndex]);
  } catch (error) {
    console.error({ error });
    res.sendStatus(404);
  }
};
