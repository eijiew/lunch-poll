import express from "express";
import { initialize } from "express-openapi";
import apiDoc from "./api/api-doc";
import * as operations from "./api/controllers";
import { Model } from "objection";
import Knex from "knex";
import cors from "cors";
import dbConfig from "../knexfile";

const knex = Knex(dbConfig);
Model.knex(knex);

const PORT = process.env.PORT || 4000;
const app = express();
app.use(express.json());
app.use(cors({ origin: ["http://localhost:3000"] }));

initialize({
  app,
  apiDoc: apiDoc as any,
  operations,
  docsPath: "/api-docs",
  errorMiddleware(err, _req, res, _next) {
    res.status(err.status).json(err.errors);
  },
});

app.listen(PORT, function () {
  console.log(`Express app listening on port ${PORT}`);
});
