# Lunch Poll App
## Getting Started

This application consists of frontend and backend app.
```
git clone git@gitlab.com:eijiew/lunch-poll.git
```

### Docker

To run the application, there is docker configuration done. Simply run the below command
```
docker compose up
```
and visit the web app at http://localhost:3000

### Breakdown
To run the application individually
### Backend
#### Installation
```
cd server
npm install
npm run build
```

#### Database setup
```
npm run migrate:db
```

#### Running the app
```
npm start
```
The app should be running at http://localhost:4000

### Frontend
#### Installation
```
cd client
npm install
```

#### Running the app
```
npm start
```
The app should be running at http://localhost:3000

## Documentations

### Architecture Decision Records

We use Architecture Decision Records (ADRs) in our project. And we keep them in `./doc/adr` directory.

Tool to create an ADRs are <https://github.com/npryce/adr-tools>.
To install it please see this guide, <https://github.com/npryce/adr-tools/blob/master/INSTALL.md>

Example of usage, once `adr-tools` installed,

```bash
adr help
adr new Implement new script
```

### Open API

API specifications can be retrieve at [/api-docs](http://localhost:4000/api-docs).
To have a more UI friendly view, you may load the spec at https://mermade.github.io/openapi-gui to view

## Additional Feature
As a democratic sociality, I added a voting feature for user to vote for their preference. This give the group an option to either go with the majority or follow the random selection. 
