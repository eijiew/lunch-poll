import React, {
  ChangeEventHandler,
  MouseEventHandler,
  useCallback,
  useEffect,
  useState,
} from "react";
import { FormEventHandler } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Option } from "../components/Option";

export const Poll: React.FC = () => {
  const username = localStorage.getItem("username");
  const { uuid } = useParams();
  const navigate = useNavigate();
  const [question, setQuestion] = useState<{
    question: string;
    created_by: string;
    resulted_option?: { uuid: string; option: string };
  }>();
  const [options, setOptions] =
    useState<Array<{ uuid: string; option: string }>>();
  const [newOption, setNewOption] = useState<string>("");
  const [votes, setVotes] = useState<Array<{ username: string }>>();

  const fetchQuestion = useCallback(async () => {
    const response = await fetch(`http://localhost:4000/questions/${uuid}`);
    if (response.ok) {
      const question = await response.json();
      setQuestion(question);
    } else {
      navigate("/poll");
    }
  }, [uuid, navigate]);
  const fetchOptions = useCallback(async () => {
    const response = await fetch(
      `http://localhost:4000/questions/${uuid}/options`
    );
    if (response.ok) {
      const options = await response.json();
      setOptions(options);
    }
  }, [uuid]);

  useEffect(() => {
    if (!username) {
      navigate("/");
    }

    fetchQuestion().then(fetchOptions);
  }, [navigate, uuid, fetchOptions, fetchQuestion, username]);

  useEffect(() => {
    const fetchVotes = async (optionUuid: string) => {
      const response = await fetch(
        `http://localhost:4000/options/${optionUuid}/votes`
      );
      if (response.ok) {
        const votes = await response.json();
        setVotes(votes);
      }
    };
    if (question?.resulted_option) {
      fetchVotes(question.resulted_option.uuid);
    }
  }, [question?.resulted_option]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    (event) => {
      setNewOption(event.target.value);
    },
    []
  );

  const handleSubmit: FormEventHandler = useCallback(
    async (event) => {
      event.preventDefault();

      await fetch(`http://localhost:4000/questions/${uuid}/options`, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ option: newOption }),
      });
      fetchOptions();
    },
    [uuid, newOption, fetchOptions]
  );

  const handleClosePoll: MouseEventHandler = useCallback(async () => {
    await fetch(`http://localhost:4000/questions/${uuid}/result`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ username }),
    });
    fetchQuestion();
  }, [uuid, username, fetchQuestion]);

  return (
    <div className="Poll">
      {username === question?.created_by && !question?.resulted_option ? (
        <div>
          <input type="button" value="Close Poll" onClick={handleClosePoll} />
        </div>
      ) : null}
      {question ? <label>Question: {question.question}</label> : "loading..."}
      {options?.map((option) => (
        <Option option={option} readOnly={!!question?.resulted_option} />
      ))}
      <hr />
      {question?.resulted_option ? (
        <div>
          <label>Result: {question.resulted_option.option}</label>
          <br />
          <label>Voted by:</label>
          {(votes ?? []).map((vote) => (
            <div>{vote.username}</div>
          ))}
        </div>
      ) : (
        <form onSubmit={handleSubmit}>
          <label>
            New Option:
            <input type="text" value={newOption} onChange={handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      )}
    </div>
  );
};
