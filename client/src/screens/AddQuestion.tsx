import React, {
  ChangeEventHandler,
  useCallback,
  useEffect,
  useState,
} from "react";
import { FormEventHandler } from "react";
import { useNavigate } from "react-router-dom";

export const AddQuestion: React.FC = () => {
  const username = localStorage.getItem("username");
  const navigate = useNavigate();
  const [question, setQuestion] = useState<string>("");

  useEffect(() => {
    if (!username) {
      navigate("/");
    }
  }, [navigate, username]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    (event) => {
      setQuestion(event.target.value);
    },
    []
  );

  const handleSubmit: FormEventHandler = useCallback(
    async (event) => {
      event.preventDefault();

      const response = await fetch("http://localhost:4000/questions", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify({ question, created_by: username }),
      });
      const { uuid } = await response.json();
      navigate(`/poll/${uuid}`);
    },
    [navigate, question, username]
  );

  return (
    <div className="AddQuestion">
      <form onSubmit={handleSubmit}>
        <label>
          Question:
          <input type="text" value={question} onChange={handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};
