import React, {
  ChangeEventHandler,
  useCallback,
  useEffect,
  useState,
} from "react";
import { FormEventHandler } from "react";
import { useNavigate } from "react-router-dom";

export const Login: React.FC = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState<string>("");

  useEffect(() => {
    const username = localStorage.getItem("username");
    if (username) {
      navigate("/poll");
    }
  }, [navigate]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    (event) => {
      setUsername(event.target.value);
    },
    []
  );

  const handleSubmit: FormEventHandler = useCallback((event) => {
    event.preventDefault();
    localStorage.setItem("username", username);
    navigate("/poll");
  }, [navigate, username]);

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <label>
          User Name:
          <input type="text" value={username} onChange={handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};
