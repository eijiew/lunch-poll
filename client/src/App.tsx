import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Login } from "./screens/Login";
import { Poll } from "./screens/Poll";
import { AddQuestion } from "./screens/AddQuestion";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/poll" element={<AddQuestion />} />
        <Route path="/poll/:uuid" element={<Poll />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
