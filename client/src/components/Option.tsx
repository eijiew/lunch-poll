import React, {
  ChangeEventHandler,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";

interface Props {
  option: {
    uuid: string;
    option: string;
  };
  readOnly: boolean;
}

export const Option: React.FC<Props> = ({ option, readOnly }) => {
  const username = localStorage.getItem("username");
  const [votes, setVotes] = useState<Array<{ username: string }>>();

  const fetchVotes = useCallback(async () => {
    const response = await fetch(
      `http://localhost:4000/options/${option.uuid}/votes`
    );
    if (response.ok) {
      const votes = await response.json();
      setVotes(votes);
    }
  }, [option.uuid]);

  useEffect(() => {
    fetchVotes();
  }, [fetchVotes]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = useCallback(
    async (event) => {
      const method = event.target.checked ? "POST" : "DELETE";
      await fetch(`http://localhost:4000/options/${option.uuid}/votes`, {
        headers: {
          "Content-Type": "application/json",
        },
        method,
        body: JSON.stringify({ username }),
      });
      fetchVotes();
    },
    [fetchVotes, option.uuid, username]
  );

  const userVoted = useMemo(() => {
    return votes?.some(({ username: votedUser }) => votedUser === username);
  }, [votes, username]);

  return (
    <div>
      <input
        type="checkbox"
        id={option.option}
        name={option.option}
        value={option.uuid}
        onChange={handleChange}
        checked={userVoted}
        disabled={readOnly}
      />
      <label htmlFor={option.option}>{option.option}</label>
      <sup>{votes?.length ?? 0} voted</sup>
    </div>
  );
};
