# 2. Use sql databse with objectionjs ORM

Date: 2023-04-04

## Status

Accepted

## Context

To store the polling data

## Decision

Decide to use sql databse such as SQLite3 and ORM library objectionJS

## Consequences

- sql databse because the model can be well-defined and the language is well-understood by many
- used ORM library, so that it helps to eliminate the risk of writing raw queries, such as SQL injection security risk
- used objectionJS with knex, because it supports multiple different database such as PostgreSQL, MySQL, OracleDB, etc. As a POC application, we started with SQLite for low cost setup but as we grow we might want to scale and use a better supported database. And this helps us to easily make the switch when needed
