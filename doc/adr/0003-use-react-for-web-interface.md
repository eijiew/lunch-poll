# 3. Use react for web interface

Date: 2023-04-05

## Status

Accepted

## Context

Having a user friendly interface for users to interact with our application

## Decision

Create a web interface using reactJS

## Consequences

- Use web interface because its the most universal and easy for our user to adopt
- Use reactJS because its light weight, flexible and easy to get started
