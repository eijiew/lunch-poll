# 1. Use expressjs with openapi

Date: 2023-04-03

## Status

Accepted

## Context

To create an application for lunch suggestions polling

## Decision

Decided to use expressjs with openapi standards to create a RESTful api service for the backend of the application

## Consequences

- REST api because its a common standards to integrate with others, and separate of concern from front (client) vs back (server)
- expressjs is lightweight and easy to implement for small application
- openapi is an open standards that is widely used for REST api services
